import React, { Component } from 'react';
import './App.css';
import DishesMenu from "../components/DishesMenu/DishesMenu";
import Basket from "../components/Basket/Basket";

class App extends Component {
    render() {
    return (
      <div className="App">
          <DishesMenu/>
          <Basket/>
      </div>
    );
  }
}


export default App;
