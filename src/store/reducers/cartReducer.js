import {ADD_DISH, ORDER_SUCCESS, REMOVE_DISH} from "../actions/actionType";

const initialState = {
    cart: {},
    totalPrice: 0,
    delivery: 150
};

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case ORDER_SUCCESS:
            let newCart = {};
            Object.keys(action.dishes).forEach(dish => {
                newCart[dish] = 0;
            });

            return {...state, cart: newCart};

        case ADD_DISH:
            return {
                ...state,
                cart: {
                    ...state.cart,
                    [action.dish.title.toLowerCase()]: state.cart[action.dish.title.toLowerCase()] + 1
                },
                totalPrice: state.totalPrice + action.dish.price
            };
        case REMOVE_DISH:
            return {
                ...state,
                cart: {
                    ...state.cart,
                    [action.dish.toLowerCase()]: state.cart[action.dish.toLowerCase()] - 1
                },
                totalPrice: state.totalPrice - action.price
            };



        default:
            return state

    }
};

export default cartReducer
