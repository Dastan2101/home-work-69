import React, {Component} from 'react';
import {addDish, getDishes} from "../../store/actions/actionCreator";
import {connect} from "react-redux";
import './DishesMenu.css';

class DishesMenu extends Component {
    componentDidMount() {
        this.props.getDishes()
    };

    render() {

        const dishesArray = Object.keys(this.props.dishes).map(name => {
            return {...this.props.dishes[name]}
        });
        let dishesForMap = dishesArray.map((dish, key) => {
            return (
                <div className="dish-card" key={key}>
                    <img src={dish.image} alt="#"
                    />
                    <h4 className="title">{dish.title}</h4>
                    <p className="title"><b>{dish.price}KGS</b></p>
                    <button type="button"
                            onClick={() => this.props.addDishes({title: dish.title, price: dish.price})}>Add dish to
                        cart
                    </button>
                </div>
            )
        });

        return (
            <div className="dishes-block">
                {dishesForMap}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    dishes: state.dishes.dishes
});

const mapDispatchToProps = dispatch => ({
    getDishes: () => dispatch(getDishes()),
    addDishes: (dish) => dispatch(addDish(dish)),
});
export default connect(mapStateToProps, mapDispatchToProps)(DishesMenu);