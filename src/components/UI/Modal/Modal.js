import React from 'react';
import {Button, Modal} from 'reactstrap';
import connect from "react-redux/es/connect/connect";
import {createOrder} from "../../../store/actions/actionCreator";
import './Modal.css';

class ModalExample extends React.Component {

    state = {
        name: '',
        tel: '',
        address: '',
    };

    constructor(props) {
        super(props);
        this.state = {
            modal: false

        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    valueChanged = event => {
        event.preventDefault();
        const {name, value} = event.target;
        this.setState({[name]: value});
    };


    orderHandler = event => {
        event.preventDefault();
        const orderPost = {
            order: this.props.cart,
            personal: {
                name: this.state.name,
                tel: this.state.tel,
                address: this.state.address
            }

        };

        this.props.createOrder(orderPost);

        this.setState({modal: false})

    };

    render() {
        return (
            <div>
                <Button color="danger" onClick={this.toggle}>Place order</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <div className="container">
                        <form onSubmit={this.orderHandler}>
                            <input className="input" type="text" name="name" placeholder="Your Name"
                                   value={this.state.name || ''} onChange={this.valueChanged}
                            />
                            <input className="input" type="text" name="tel" placeholder="Your Tel Number"
                                   value={this.state.tel || ''} onChange={this.valueChanged}
                            />
                            <input className="input" type="text" name="address" placeholder="address"
                                   value={this.state.address || ''} onChange={this.valueChanged}
                            />
                            <Button btntype="Success">Create Order</Button>
                        </form>
                    </div>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    cart: state.cart.cart,
});

const mapDispatchToProps = dispatch => ({
    createOrder: (orderPost) => dispatch(createOrder(orderPost)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalExample);