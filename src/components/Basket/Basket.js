import React, {Component} from 'react';
import {connect} from "react-redux";
import './Basket.css';
import {removeDish} from "../../store/actions/actionCreator";
import Modal from "../UI/Modal/Modal";

class Basket extends Component {

    render() {

        const ordersArray = Object.keys(this.props.dishes).map(order => {
            if (this.props.cart[order] < 1) {
                return null
            }


            return (
                <div className="basket-card" key={order}>
                    <h2 className="basket-card_title">Dish : {this.props.dishes[order].title}</h2>
                    <h4 className="basket-card_title">Quantity : {this.props.cart[order]}</h4>
                    <h3 className="basket-card_title">Price
                        : {this.props.dishes[order].price * this.props.cart[order]}</h3>
                    <button type="button"
                            onClick={() => this.props.removeDish(this.props.dishes[order].title, this.props.dishes[order].price)}>Remove
                    </button>
                </div>
            )

        });

        return (

            <div className="basket-block">
                {ordersArray}
                <div className="delivery">
                    <h3>Доставка : {this.props.delivery}</h3>
                    <h2 className="total-price">Total price: {this.props.totalPrice + this.props.delivery}</h2>
                    <Modal/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    cart: state.cart.cart,
    dishes: state.dishes.dishes,
    totalPrice: state.cart.totalPrice,
    delivery: state.cart.delivery
});

const mapDispatchToProps = dispatch => ({
    removeDish: (dish, price) => dispatch(removeDish(dish, price))
});


export default connect(mapStateToProps, mapDispatchToProps)(Basket);